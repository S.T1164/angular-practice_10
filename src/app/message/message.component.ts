import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { MycheckService } from '../mycheck.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
  providers: [MycheckService],
})
export class MessageComponent implements OnInit {

  content : string[];
  @Output() action = new EventEmitter<MouseEvent>();

  constructor(private service : MycheckService,
              private route : ActivatedRoute) {
    service.push('message data');
   }

  ngOnInit(): void {
   this.service.push('params: ' + JSON.stringify(this.route.snapshot.paramMap));
   this.content = this.service.list;
  }

  doAction(event){
    this.action.emit(event);
  }

  pop(){
    this.content.pop();
  }

}
