import { Component, OnInit } from '@angular/core';
import { MycheckService } from '../mycheck.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  title : string;
  message : string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.title = 'Hello-app';
    this.message = 'param: ' + JSON.stringify(this.route.snapshot.paramMap);
  }

}
